# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# Do not forget to open an action server by typing the command "rasa run actions"

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.events import SlotSet
from rasa_sdk.executor import CollectingDispatcher
from datetime import date
import sqlite3

datapath = "/Users/titouannguyenhuynh/Desktop/Rasa/models/data/BDD.db"
conn = sqlite3.connect(datapath)
cursor = conn.cursor()

class ActionSignalMail(Action):

    def name(self) -> Text:
        return "action_signal_mail"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        mailvalue = next(tracker.get_latest_entity_values("gender"),None)
        name = tracker.slots.get("gender")
        mailvalue = str(mailvalue)
        text = "Votre adresse mail est "+ mailvalue + str(name)
        dispatcher.utter_message(text)

        return []

class ActionSetUserData(Action):

    def name(self) -> Text:
        return "set_user_data"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        userID = tracker.slots.get("social_security_number")
        nom = tracker.slots.get("name")
        date = tracker.slots.get("birth_date")
        sexe = tracker.slots.get("gender")
        chronique = tracker.slots.get("medical_past")

        cursor.execute(f"INSERT INTO User_Stat VALUES ('{userID}','{nom}', '{date}','{sexe}','{chronique}')")
        conn.commit()
        return []

class ActionGetUserData(Action):

    def name(self) -> Text:
        return "get_user_data"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        userID = tracker.slots.get("social_security_number")

        cursor.execute(f'SELECT * FROM User_Stat WHERE UserId = "{userID}"')
        entry = cursor.fetchone()

        SlotSet("name",str(entry[1]))
        SlotSet("birth_date",str(entry[2]))
        SlotSet("gender",str(entry[3]))
        SlotSet("medical_past",str(entry[4]))
        SlotSet("already_registered",True)

        dispatcher.utter_message(text=f"Welcome back {entry[1]}! Are you ready to answer our health form?")

        return [SlotSet("name",str(entry[1])),SlotSet("birth_date",str(entry[2])),SlotSet("gender",str(entry[3])),SlotSet("medical_past",str(entry[4]))]

class ActionSetLatestHealthForm(Action):

    def name(self) -> Text:
        return "set_latest_health_form"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        userID = tracker.slots.get("social_security_number")
        Date = date.today().strftime("%d/%m/%Y")
        localisation = tracker.slots.get("location")
        SleepingTime = tracker.slots.get("start_sleep")
        WakeUpTime = tracker.slots.get("end_sleep")

        cursor.execute(f"INSERT INTO User_Dynam VALUES ('{userID}', '{Date}','{localisation}','{SleepingTime}','{WakeUpTime}')")
        conn.commit()
        return []

class TestDb(Action):

    def name(self) -> Text:
        return "test_db"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:


        #cursor.execute(f"INSERT INTO User_Stat VALUES ('1111111111111','John Doe', '05/01/2002','male','I had chronic pneumonia')")
        #conn.commit()
        dispatcher.utter_message(text=str(tracker.slots.get("birth_date")))

        return []